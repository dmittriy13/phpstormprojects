<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 06.10.17
 * Time: 3:52
 */

define("coefficientLike", 1.0);
define("coefficientPost", 0.5);

require_once "classes/User.php";
require_once "classes/Like.php";
require_once "classes/Post.php";
require_once "classes/Tools.php";


$json = Tools::readJsonFile("json/likes.json");

//print_r($json);
$likes = array();
foreach ($json["likes"] as $item) {
    array_push($likes, new Like($item["id"], $item["user_id"], $item["post_id"]));
}

$json = Tools::readJsonFile("json/posts.json");
$posts = array();
foreach ($json["posts"] as $item) {
    array_push($posts, new Post($item["id"], $item["user_id"], $item["title"]));
}

$json = Tools::readJsonFile("json/users.json");
$users = array();
foreach ($json["users"] as $item) {
    array_push($users, new User($item["id"], $item["name"], $item["surname"]));
}

foreach ($users as $user) {
    $postsCurUser = $user->getPosts($posts);
    $countLike = 0;
    foreach ($postsCurUser as $postCurUser){
        foreach ($likes as $like) {
            if ($like->postId == $postCurUser->id and $like->userId != $user->id) {
                $countLike++;
            }
        }
    }
    $user->rating =  (coefficientPost * count($postsCurUser)) + (coefficientLike * $countLike);
}

uasort($users, function ($a, $b) {
    return $b->rating - $a->rating;
});