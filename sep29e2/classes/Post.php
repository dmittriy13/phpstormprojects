<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.09.17
 * Time: 10:58
 */

class Post {
    public $id = null;
    public $userId = null;
    public $title = null;

    public $user = null;

    /**
     * Post constructor.
     * @param $id
     * @param $userId
     * @param $title
     */
    public function __construct($id, $userId, $title) {
        if (isset($id))
            $this->id = $id;
        if (isset($userId))
            $this->userId = $userId;
        if (isset($title))
            $this->title = $title;
    }

    /**
     * @param array $users
     * @return mixed|null
     */
    public function getUser($users = array()) {
        if(isset($users)) {
            foreach ($users as $user) {
                if ($user->id == $this->userId) {
                    $this->user = $user;
                    return $this->user;
                }
            }
        }


    }
}