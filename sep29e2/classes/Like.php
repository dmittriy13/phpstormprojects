<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.09.17
 * Time: 10:58
 */

class Like {
    public $Id = null;
    public $userId = null;
    public $postId = null;

    public $user = null;
    public $post = null;

    /**
     * Like constructor.
     * @param $Id
     * @param $userId
     * @param $postId
     */
    public function __construct($Id, $userId, $postId)
    {
        if (isset($Id))
            $this->Id = $Id;
        if (isset($userId))
            $this->userId = $userId;
        if (isset($postId))
            $this->postId = $postId;
    }

    /**
     * @param array $users
     * @return mixed|null
     */
    public function getUser($users = array()) {

        foreach ($users as $user) {
            if ($user->id == $this->userId) {
                $this->user = $user;
                return $this->user;
            }
        }
        return null;
    }

    /**
     * @param array $posts
     * @return mixed|null
     */
    public function getPost($posts = array()) {
        foreach ($posts as $post) {
            if ($post->id == $this->postId) {
                $this->post = $post;
                return $this->post;
            }
        }
        return null;
    }
}