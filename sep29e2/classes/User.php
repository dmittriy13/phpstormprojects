<?php
/**
 * Created by PhpStorm.
 * User: student
 * Date: 29.09.17
 * Time: 10:58
 */

class User {
    public $id = null;
    public $name = null;
    public $surname = null;
    public $rating = null;

    public $posts = array();

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $surname
     */
    public function __construct($id, $name, $surname) {
        if (isset($id))
            $this->id = $id;
        if (isset($name))
            $this->name = $name;
        if (isset($surname))
            $this->surname = $surname;

    }

    /**
     * @param array $posts
     * @return array
     */
    public function getPosts($posts = array()) {
        foreach ($posts as $post) {
            if ($post->userId == $this->id) {
                array_push($this->posts, $post);
            }
        }

        return $posts;
    }
}