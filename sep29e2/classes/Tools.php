<?php
/**
 * Created by PhpStorm.
 * User: dmitriy
 * Date: 30.09.17
 * Time: 18:52
 */
class Tools
{
    public static function readJsonFile($file) {
        if (isset($file)) {
            $jsonStr = file_get_contents($file);
            if (!$jsonStr){
                echo 'error read file!';
                return null;
            }
            return json_decode($jsonStr, true);
        }
    }
}