<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>rating</title>
    <script>
        <?php
            require_once "main.php";
        ?>
    </script>
</head>
<body>
<table border="1">
    <tr>
        <td> Name </td>
        <td> Rating </td>
    </tr>
    <?php foreach($users as $user) : ?>
        <tr>
            <td> <?php echo $user->name; ?> </td>
            <td> <?php echo  $user->rating; ?> </td>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>